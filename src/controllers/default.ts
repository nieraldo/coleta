import { Pessoa } from '../interfaces/Pessoa'

export function index(req, res) {

    let eu: Pessoa = { nome: 'nieraldo', idade: 47 }

    function imprime(eu: Pessoa) {
        return eu
    }

    return res.send(JSON.stringify(imprime(eu)));
}